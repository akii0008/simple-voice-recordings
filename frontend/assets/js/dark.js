// When the DOM content loads, if the user had dark mode enabled before, enable it now.
// Credit: https://flaviocopes.com/dark-mode/
document.addEventListener('DOMContentLoaded', async () => {
  ((localStorage.getItem('mode') || 'dark') === 'dark')
    ? document.querySelector('body').classList.add('dark')
    : document.querySelector('body').classList.remove('dark');

  // Wait to load everything, then add the style.
  // Otherwise, if it's default dark mode, we blind the user because it loads light first.
  setTimeout(() => 
    document.body.style = 'transition: background-color 250ms ease-in-out;', 
  500);
});

let timesDarkWasPressed = 0;
async function toggleDark() { // eslint-disable-line no-unused-vars
  const bodyClassList = document.getElementById('body').classList;

  function addDark() {
    console.log('👻 dark boi: ' + (timesDarkWasPressed + 1));
    bodyClassList.add('dark');
    localStorage.setItem('mode', 'dark');
  }
  function removeDark() {
    console.log('☀ light boi: ' + (timesDarkWasPressed + 1));
    bodyClassList.remove('dark');
    localStorage.setItem('mode', 'light');
  }

  bodyClassList.contains('dark') ? removeDark() : addDark();
  timesDarkWasPressed++; // Count the user's clicks..

  if (timesDarkWasPressed === 100) {
    timesDarkWasPressed = 0;
    return window.location = 'https://www.youtube.com/watch?v=dQw4w9WgXcQ'; // rick rolled ;)
  }
}
