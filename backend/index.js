const express = require('express');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const helmet = require('helmet');
const ffmpeg = require('fluent-ffmpeg');
const rateLimit = require('express-rate-limit');
const { rm } = require('fs');
const { readFile } = require('fs/promises');

const app = express();

// enable files upload
app.use(fileUpload({
  createParentPath: true
}));

const whitelist = ['https://svr.akii.dev', 'http://192.168.1.228', 'http://127.0.0.1:5500'];
const corsOptions = {
  origin: function (origin, callback) {
    console.log('Origin:', origin);
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      callback(new Error('Not allowed by CORS'));
    }
  }
};

// add other middleware
app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan(':date[iso] :remote-addr :method :url :status :response-time ms'));
app.use(helmet());
app.use('/', express.static('uploads'));
app.set('trust proxy', 1);

// start app
const PORT = process.env.PORT || process.env.NODE_ENV === 'production' ? 3000 : 3080;

app.listen(PORT, () =>
  console.log(`App is listening on port ${PORT}.`)
);

// Allow 2 requests per 5 seconds
const limiter = rateLimit({
  windowMs: 5000,
  max: 2,
  message: JSON.stringify({ error: true, message: 'Too many requests. Try again in 5 seconds.' })
});

let updateCache = '';
setInterval(() => {
  console.log('Clearing cached update...');
  updateCache = '';
}, 3.6e+6); // 1 hour
app.get('/update', limiter, async (req, res) => {

  // There was an update cached earlier, so use that instead of polling the system
  if (updateCache) {
    console.log('Sending cached update...');
    return res.send({
      error: false,
      update: true,
      message: updateCache
    });
  }

  console.log('No cached update. Sending now...');
  readFile('./update.txt', { encoding: 'utf-8' })
    .then(data => {

      // Cache the data so we're not polling the system every time
      // Really this would be better in the browser but whatever
      updateCache = data;

      return res.send({
        error: false,
        update: true,
        message: data
      });
    }).catch(e => {
      // File does not exist; there is no update
      if (e.code === 'ENOENT') {
        return res.send({
          error: false,
          update: false
        });
      } else {
        console.error(e.stack || e.message || e);
        return res.status(500).send({
          error: true,
          update: false,
          message: e.message || e
        });
      }
    });
});

app.post('/upload', limiter, async (req, res) => {
  try {

    if (!req.files) {
      res.status(400).send({
        error: true,
        message: 'No file uploaded'
      });
    } else {
      // Use the name of the input field (i.e. "recording") to retrieve the uploaded file
      const recording = req.files.recording;

      const randomID = makeid(5);
      const oggPath = './uploads/' + randomID + '.ogg';
      const mp3Path = './uploads/' + randomID + '.mp3';

      // Use the mv() method to place the file in upload directory (i.e. "uploads")
      await recording.mv(oggPath);

      // Convert the OGG to MP3 so it's able to be used by more browsers
      convert(oggPath, mp3Path, convertError => {
        if (convertError) {
          console.error(convertError);
          return res.status(500).send({
            error: true,
            message: 'Error converting file',
            path: `${randomID}.mp3`,
            emsg: convertError
          });
        }

        // Then remove the original OGG file
        rm(oggPath, removeError => { // fuck i hate callback hell so much
          if (removeError) {
            console.error(removeError);
            return res.status(500).send({
              error: true,
              message: 'Error removing file',
              path: `${randomID}.mp3`,
              emsg: removeError
            });
          }

          // And return with a success!
          return res.send({
            error: false,
            message: 'Uploaded file',
            path: `${randomID}.mp3`
          });
        });
      });


    }
  } catch (err) {
    console.error(err);
    res.status(500).send(err);
  }
});

// Error 404 handler
app.use((req, res) => res.status(404).send({ error: true, message: '404: Unknown endpoint' }));

// Error 500 handler
// eslint-disable-next-line no-unused-vars
app.use(function (err, req, res, next) { // Don't ask me why adding next makes this block work
  // Handles a syntax error in the body of the request
  if (err instanceof SyntaxError || (err.type && err.type === 'entity.parse.failed')) {
    return res.status(400).send({ error: true, message: 'Malformed request body' });
  }

  console.error(err);
  return res.status(500).send({ error: true, message: 'Error 500: Internal server error' });
});

// Makes a random string of characters {length} characters long
function makeid(length) {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

// Converts OGG to MP3 using ffmpeg (npm package fluent-ffmpeg)
function convert(input, output, callback) {
  ffmpeg(input)
    .output(output)
    .noVideo() // disable video output.. although I might add something later ;)
    .on('end', function () {
      console.log(`Converted ${input} to ${output}`);
      callback(null);
    }).on('error', function (err) {
      callback(err);
    }).run();
}
